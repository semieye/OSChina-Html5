define(['zepto', 'underscore', 'backbone', 'text!base/components/footer-menu.html', '../login/login', '../util', "../openapi"], function($, _, Backbone, MenuHtml, Login, Util, OpenAPI) {

	var Menu = Backbone.View.extend({
		login: null,

		initialize: function() {
			this.render();
		},

		render: function() {
			$(this.el).addClass("bar-tab");
			$(this.el).html(MenuHtml);
			$(this.el).find("li").removeClass("active");

			var module = $(this.el).attr("data-value");

			$(this.el).find("li[data-module=" + module + "]").addClass("active");
			
			//轮循切换Menu时BUG。
			var me = this;
			var MenuPrompt = Piece.Session.loadObject('Prompt');
			if (MenuPrompt != null) {
				var userPromptNum = MenuPrompt.replyCount + MenuPrompt.msgCount + MenuPrompt.referCount;
				if (userPromptNum != 0) {
					$(me.el).find('.userPrompt').css("display", "block");
					$(me.el).find('.userPromptNum').html(userPromptNum);
				}
			}

			$(this.el).find("li").bind('click', function() {
				//判断更多操作是否show，如果show，先收起来
				if ($(this).attr("data-module") !== "more") {
					if ($('#footer-more-detail').get(0).style.display == "block") {
						$('#footer-more-detail').hide();
						$("#more-detail-mask").hide();
						return;
					}
				}
				
				var url = $(this).attr("data-value");
				if (url) {
					window.localStorage["footer-menu-index"] = url;
					Backbone.history.navigate(url, {
						trigger: true
					});
				}
			});
			//我的空间，未读信息
			var user_token = Piece.Store.loadObject("user_token");
			var user_message = Piece.Store.loadObject("user_message");
			// simona--add mask
			$("body").append('<div id="more-detail-mask" ></div>');

			/*更多模块的事件*/
			/*切换登陆状态*/
			$(this.el).find("#footer-more").click(function() {
				//记录返回页面
				var fulUrl = window.location.href;
				url = fulUrl.split("#");
				if(typeof url[1] == "undefined") {
					url[1] = "news/news-list";
				}
				Piece.Session.saveObject("osLastPage",url[1]);
				var user_token = Piece.Store.loadObject("user_token");
				if (!user_token || user_token === null) {
					$(".footSign").html("");
					$(".footSign").html("<span class='icon-user icon-2x'></span><div>用户登录</div>");
					$(".footSign").attr({
						"data-sign": "signin"
					});
				} else {
					$(".footSign").html("");
					$(".footSign").html("<span class='icon-power-off icon-2x'></span><div>注销登录</div>");
					$(".footSign").attr({
						"data-sign": "signout"
					});
				}
                
                //判断是否是ios，如果是的话，就隐藏“退出程序”
				if (navigator.userAgent.indexOf("iPhone") > -1) {
						$("#footSignout").hide();

					}

				$('#footer-more-detail').toggle();
				$("#more-detail-mask").toggle();

				$('#more-detail-mask').click(function() {
					$('#footer-more-detail').hide();
					$("#more-detail-mask").hide();
				});

				$(".first_row div").click(function() {
					$("#more-detail-mask").hide();
				});

				

			});

			/*我的资料*/
			$(this.el).find("#footMyInfo").click(function() {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login = new Login();
					login.show();
				} else {
					Backbone.history.navigate("user/user-info", {
						trigger: true
					});
				}
			});
			/*开源软件*/
			$(this.el).find("#footOpenSource").click(function() {
				Backbone.history.navigate("project/software-list", {
					trigger: true
				});
			});
			/*搜索*/
			$(this.el).find("#footSearch").click(function() {
				/*跳转到搜索页面，如果localstorage有值 则先删除，防止其从localstorage获取搜索条件*/
				var softwareSearch = Piece.Store.loadObject("software-search");
				if (softwareSearch) {
					Piece.Store.deleteObject("software-search");
				}
				Backbone.history.navigate("news/news-search", {
					trigger: true
				});
			});
			// 系统设置
			$(this.el).find('#footSysSetting').click(function() {
				Backbone.history.navigate("user/systemSettings", {
					trigger: true
				});
			})
			/*退出程序*/
			$(this.el).find("#footSignout").click(function() {
				// navigator.app.exitApp()
				// simona--modify--confirm退出程序
				var onConfirm = function(buttonIndex) {
					if (buttonIndex == 2) {
						navigator.app.exitApp();
					}
				};
				
					navigator.notification.confirm(
						'确定退出程序吗？', // message
						onConfirm, // callback to invoke with index of button pressed
						'提示', // title
						['取消', '确定'] // buttonLabels
					);
				
			});
			/*注销或登录*/
			$(this.el).find(".footSign").click(function() {
				var dataSign = $(".footSign").attr("data-sign");
				/*注销*/
				if (dataSign === "signout") {
					Piece.Store.deleteObject("user_token");
					Piece.Store.deleteObject("user_message");
					// Piece.Store.deleteObject("user_info");
					$('#footer-more-detail').hide();
					// 注销时清除我的空间所有列表的缓存
					Util.cleanStoreListData("my-atme-list");
					Util.cleanStoreListData("my-my-comment-list");
					Util.cleanStoreListData("my-critic-list");
					Util.cleanStoreListData("my-my-list");
					Util.cleanStoreListData("my-myself-list");
					new Piece.Toast("已退出登录");
					// 取消取消轮循 且清除轮循的缓存
					Util.roundRobin();
					Piece.Session.deleteObject('Prompt');
				}
				/*登录*/
				else {
					login = new Login();
					login.show();
				}
			});



		}
	}, {
		compile: function(contentEl) {
			var me = this;
			return _.map($(contentEl).find("footer.footer-menu"), function(tag) {
				var menu = new Menu({
					el: tag
				});
				return menu;
			});
		}
	});

	return Menu;

});