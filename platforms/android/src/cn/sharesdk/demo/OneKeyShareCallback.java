/*
 * 官网地站:http://www.ShareSDK.cn
 * 技术支持QQ: 4006852216
 * 官方微信:ShareSDK   （如果发布新版本的话，我们将会第一时间通过微信将版本更新内容推送给您。如果使用过程中有任何问题，也可以通过微信与我们取得联系，我们将会在24小时内给予回复）
 *
 * Copyright (c) 2013年 ShareSDK.cn. All rights reserved.
 */

package cn.sharesdk.demo;

import java.util.HashMap;

import android.app.Activity;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;

/**
 * OneKeyShareCallback是快捷分享功能的一个“外部回调”示例。 演示了如何通过添加extra的方法，将快捷分享的分享结果回调到 外面来做自定义处理。
 */
public class OneKeyShareCallback implements PlatformActionListener {
	private Activity mContext;

	public OneKeyShareCallback(Activity mContext) {
		super();
		this.mContext = mContext;
	}

	public void onComplete(Platform plat, int action, HashMap<String, Object> res) {
		mContext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(mContext, "分享成功", Toast.LENGTH_SHORT).show();
			}
		});
		System.out.println(res.toString());
		// 在这里添加分享成功的处理代码
	}

	public void onError(Platform plat, int action, Throwable t) {
		t.printStackTrace();
		mContext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(mContext, "分享失败", Toast.LENGTH_SHORT).show();
			}
		});

		// 在这里添加分享失败的处理代码
	}

	public void onCancel(Platform plat, int action) {
		mContext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(mContext, "取消分享", Toast.LENGTH_SHORT).show();
			}
		});

		// 在这里添加取消分享的处理代码
	}

}
